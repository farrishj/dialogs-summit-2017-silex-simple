gosass_version="0.8.5"
gosass_authors="Jared Farrish <jf@dialogs.com>"

echo "Starting SASS configuration...".
echo "Version: {$gosass_version}".

if [[ -f "./gosass.installed" && ! "$1" = "-force" ]]; then
    echo "gosass.installed file found, please remove before re-running."
    exit 0
fi

if [[ -f "./gosass.installed" && "$1" = "-force" ]]; then
    echo "gosass.installed file found, '-force' flag means it runs anyways."
fi

dt=$(date +"%Y-%m-%d %H:%M")
echo "Date: $dt
Version: $gosass_version
Authors: $gosass_authors
" > "./gosass.installed"

if [[ ! -f "./yarn.lock" ]]; then
    echo "Initializing yarn...";
    yarn init -y
fi

if [[ ! -x "./node_modules/.bin/gulp" ]]; then
    echo "Adding Gulp and other libraries..."
    yarn add gulp
    yarn add gulp-tap
    yarn add gulp-sass
    yarn add gulp-minifier
    yarn add gulp-uglify
    yarn add gulp-sourcemaps
    yarn add jquery
    yarn add bootstrap-sass@3.3.7 --exact
fi

if [[ ! -d "./web/assets" ]]; then
    echo "Adding /assets directory..."
    mkdir -m 755 ./web/assets
else
    echo "./web/assets already found.";
fi

if [[ ! -d "./web/assets/stylesheets" ]]; then
    echo "Adding /assets/stylesheets directory..."
    mkdir -m 755 ./web/assets/stylesheets
else
    echo "./web/assets/stylesheets already found.";
fi

if [[ ! -d "./web/assets/sources" ]]; then
    echo "Adding /assets/sources directory...";
    mkdir -m 755 ./web/assets/sources
else
    echo "./web/assets/sources already found.";
fi

if [[ ! -d "./web/assets/stylesheets/scss" ]]; then
    echo "Adding /assets/stylesheets/scss directory..."
    mkdir -m 755 ./web/assets/stylesheets/scss
else
    echo "./web/assets/stylesheets/scss already found.";
fi

if [[ ! -d "./web/assets/scripts" ]]; then
    echo "Adding /assets/scripts directory..."
    mkdir -m 755 ./web/assets/scripts
else
    echo "./web/assets/scripts already found.";
fi

if [[ ! -d "./web/assets/scripts/dist" ]]; then
    echo "Adding /assets/scripts/dist directory..."
    mkdir -m 755 ./web/assets/scripts/dist
else
    echo "./web/assets/scripts already found.";
fi

if [[ ! -f "./web/assets/stylesheets/scss/_variables.scss" ]]; then
    cat > "$PWD/web/assets/stylesheets/scss/_variables.scss" <<- EOM
\$font-size-base:          14px !default;
\$font-size-large:         ceil((\$font-size-base * 1.25)) !default;
\$font-size-small:         ceil((\$font-size-base * 0.85)) !default;

\$font-size-h1:            floor((\$font-size-base * 2.6)) !default;
\$font-size-h2:            floor((\$font-size-base * 2.15)) !default;
\$font-size-h3:            ceil((\$font-size-base * 1.7)) !default;
\$font-size-h4:            ceil((\$font-size-base * 1.25)) !default;
\$font-size-h5:            \$font-size-base !default;
\$font-size-h6:            ceil((\$font-size-base * 0.85)) !default;
EOM
else
    echo "./web/assets/stylesheets/scss/_variables.scss already found.";
fi

if [[ ! -f "./web/assets/stylesheets/scss/site.scss" ]]; then
    cat > "$PWD/web/assets/stylesheets/scss/site.scss" <<- EOM
@import "variables";
@import "./web/assets/sources/bootstrap-sass/assets/stylesheets/bootstrap";
EOM
else
    echo "./web/assets/stylesheets/scss/site.css already found.";
fi

if [[ ! -d "./web/assets/stylesheets/compiled" ]]; then
    echo "Adding /assets/stylesheets/compiled directory..."
    mkdir -m 755 ./web/assets/stylesheets/compiled
else
    echo "./web/assets/stylesheets/compiled already found.";
fi

if [[ ! -L "./gulp" && ! -x "./gulp" ]]; then
    echo "Creating Gulp executable symlink..."
    ln -s "$PWD/node_modules/.bin/gulp" "$PWD/gulp"
else
    echo "$PWD/gulp executable symlink already found.";
fi

if [[ ! -x "./web/assets/sources/bootstrap-sass" ]]; then
    echo "Creating Bootstrap Sass source directory symlink..."
    ln -s "$PWD/node_modules/bootstrap-sass" "$PWD/web/assets/sources/bootstrap-sass"
else
    echo "./web/assets/sources/bootstrap-sass symlink already found.";
fi

if [[ ! -f "./Gulpfile.js" ]]; then
    cat > "$PWD/Gulpfile.js" <<- EOM
/* gosass.sh */

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minify = require('gulp-minifier'),
    sourcemaps = require('gulp-sourcemaps');

var tasks = [],
    config = {
        jqueryDir: process.cwd()+'/node_modules/jquery',
        publicDir: process.cwd()+'/web/assets',
        bootstrapPublicDir: process.cwd()+'/web/assets/sources/bootstrap-sass',
    };

ctlog('Working directory '+process.cwd());
ctlog('argv: '+JSON.stringify(process.argv));

gulp.task('prod', function(){
    tasks.push('prod');
    tasks.push('fonts');
    tasks.push('jquery-scripts');
    tasks.push('bootstrap-scripts');

    ctlog('Working directory '+process.cwd());

    return gulp
        .src(config.publicDir+'/stylesheets/scss/*.scss')
        .pipe(sass({
            includePaths: [
                config.bootstrapPublicDir,
            ],
        }).on('error', sass.logError))
        .pipe(minify({
            minify: true,
            collapseWhitespace: true,
            minifyJS: false,
            minifyCSS: true,
        }))
        .pipe(gulp.dest(config.publicDir+'/stylesheets/compiled'))
    ;
});

gulp.task('dev', function(){
    tasks.push('dev');

    ctlog('Working directory '+process.cwd());

    return gulp
        .src(config.publicDir+'/stylesheets/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: [
                config.bootstrapPublicDir,
            ],
        }).on('error', sass.logError))
        .pipe(sourcemaps.write('.', {
            sourceMappingURLPrefix: '/assets/stylesheets/compiled',
        }))
        .pipe(gulp.dest(config.publicDir+'/stylesheets/compiled'))
    ;
});

gulp.task('fonts', function() {
    tasks.push('fonts');

    return gulp
        .src(config.bootstrapPublicDir+'/assets/fonts/**/*')
        .pipe(gulp.dest(config.publicDir+'/fonts'));
});

gulp.task('jquery-scripts', function() {
    tasks.push('jquery-scripts');

    return gulp
        .src(config.jqueryDir+'/dist/*.js')
        .pipe(gulp.dest(config.publicDir+'/scripts/dist/jquery'));
});

gulp.task('bootstrap-scripts', function() {
    tasks.push('bootstrap-scripts');

    return gulp
        .src(config.bootstrapPublicDir+'/assets/javascripts/*.js')
        .pipe(gulp.dest(config.publicDir+'/scripts/dist/bootstrap'));
});

gulp.task('watch', function(){
    tasks.push('dev');
    tasks.push('watch');

    ctlog("Now watching 'dev' task ...");

    gulp.watch([config.publicDir+'/stylesheets/scss/*.scss'], ['dev']);
});

gulp.task('default', tasks);

function has_task_switch(task_name) {
    return has_switch('-d'+task_name);
}

function has_switch(flag) {
    return process.argv.indexOf(flag) >= 0;
}

function ctlog(text) {
    var dt = new Date();

    //process.stdout.write(
    console.log(
        '['+
            zo(dt.getHours())+':'+
            zo(dt.getMinutes())+':'+
            zo(dt.getSeconds())+
        '] '+
        text
    );

    function zo(n){
        return (n < 10 ? '0' : '')+n;
    }
}
EOM
else
    echo "./Gulpfile.js already found.";
fi

echo "Finished!"
