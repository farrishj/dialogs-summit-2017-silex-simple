<?php

namespace App;

// web/index.php
require_once __DIR__ . '/../vendor/autoload.php';

use App\Repository\Page;
use App\Repository\PageRepository;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Silex\Application;
use Silex\Provider\CsrfServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\LocaleServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Symfony\Component\Debug\Debug;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\CsrfToken;

if (!defined('APP_ENV')) {
   define('APP_ENV', 'prod');
}

// Only need a variable if we need to use it from this scope.
new App(new Application());

class App {
    const TEMPLATE_DIR = 'templates/';
    protected $app = null;
    
    public function __construct(Application $app)
    {
        // This seems like it should extends Silex\Application but got errors.
        $this->app = $app;
        
        $this->setEnvironment();
        
        $this->registerServices();
        $this->registerSecurity();
        $this->registerDebug();
        $this->registerControllers();
        
        // Let 'er rip.
        $this->app->run();
    }
    
    protected function setEnvironment()
    {
        // This is here because Flysystem removes the path's prefix '/',
        // so the path needs be reduced so root_path works right (no expansion).
        chdir(__DIR__.'/../');
        $this->app['filesystem.root_path'] = getcwd();
    }
    
    protected function registerDebug()
    {
        // Configure the Debug and web profiler.
        $this->app['debug'] = APP_ENV === 'dev';
        
        if ($this->app['debug']) {
            Debug::enable();
        
            $this->app->register(new WebProfilerServiceProvider(), array(
                'profiler.cache_dir' => 'cache/profiler',
                'profiler.mount_prefix' => '/_profiler', // this is the default
            ));
        }
    }
    
    protected function registerServices()
    {
        // Get some services going.
        $this->app->register(new SessionServiceProvider());
        $this->app->register(new ServiceControllerServiceProvider());
        // Locale and Translation are required for Form Service.
        $this->app->register(new LocaleServiceProvider());
        $this->app->register(new TranslationServiceProvider(), [
            'locale_fallbacks' => ['en'],
            'translator.messages' => [],
            'translator.domains' => [],
        ]);
        $this->app->register(new AssetServiceProvider());
        $this->app->register(new TwigServiceProvider(), [
            'twig.path' => self::TEMPLATE_DIR
        ]);
        $this->app->register(new FormServiceProvider());
        $this->app->register(new HttpFragmentServiceProvider());
        $this->app->register(new MonologServiceProvider());
        
        // Some of our own... On the cheap. These should be
        // ProviderInterface, which call register().
        $this->app['filesystem.local'] = new Filesystem(new Local(
            $this->app['filesystem.root_path']
        ));
        $this->app['app.repos.page'] = new PageRepository(
            $this->app,
            $this->app['filesystem.local']
        );
        $this->app['app.repos.page']->fetch();
    }
    
    protected function registerSecurity()
    {
        $users = [
            'admin' => [
                'ROLE_ADMIN',
                '$2y$10$3i9/lVd8UOFIJ6PAMFt8gu3/r5g0qeCJvoSlLCsvMTythye19F77a',
            ],
            'jared' => [
                'ROLE_ADMIN',
                '$2y$10$HtQPKsbSrffBscX5yTnppemLSHpyGgFFOw/QldW7m8uzlKCLB.9VC',
            ],
        ];
        
        $firewalls = [
            'security.firewalls' => [
                'admin' => [
                    'pattern' => '^/admin/',
//                    'form' => [
//                        'login_path' => '/user/login',
//                        'check_path' => '/admin/login_check',
//                    ],
//                    'logout' => [
//                        'logout_path' => '/admin/logout',
//                    ],
                    'http' => true,
                    'users' => [
                        'admin' => $users['admin'],
                        'jared' => $users['jared'],
                    ],
                ],
//                'dev' => [
//                    'pattern' => '^/(_(profiler|wdt)|css|images|js)/',
//                    'http' => true,
//                    'users' => [
//                        'admin' => $users['admin'],
//                        'jared' => $users['jared'],
//                    ],
//                ],
            ],
        ];
    
        $this->app->register(new SecurityServiceProvider(), $firewalls);
        $this->app->register(new CsrfServiceProvider());
    }
    
    protected function registerControllers()
    {
        $app = $this->app;
        
        $page_list_func = function (Request $req) use ($app) {
            return $app['twig']->render('page_list.html.twig', [
                'pages' => $app['app.repos.page']->sortByDate(
                    $req->query->get('dir') ?: 'ASC'
                ),
            ]);
        };
    
        $app->before(function (Request $request) {
            $request->getSession()->start();
            
            if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
                $data = json_decode($request->getContent(), true);
                $request->request->replace(is_array($data) ? $data : []);
            }
        });
        
        // Controllers
        $this->app->get('/', $page_list_func)->bind('homepage');
        $this->app->get('/page/list', $page_list_func)->bind('page_list');
    
        $this->app->get('/list/json', function (Response $res) use($app) {
            return $this->app->json([
                'pages' => $app['app.repos.page']->serialize(),
            ]);
        })->bind('page_list_json');
    
        $this->app->get('/{slug}', function ($slug) use ($app) {
            return $app['twig']->render('page_view_plain.html.twig', [
                'page' => $app['app.repos.page']->findBySlug($slug)
            ]);
        })->bind('page_by_slug');
    
        $this->app->get('/{slug}/json', function ($slug) use ($app) {
            /* @var Response $response */
            $response = new JsonResponse([
                'page' => $app['app.repos.page']->findBySlug($slug)->serialize()
            ]);
            
            $response->headers->set('Content-Type', ['application/json']);
            
            return $response;
        })->bind('page_by_slug_json');
        
        $app->get('/user/login', function(Request $request) use ($app) {
            return $app['twig']->render('login.html.twig', [
                'error' => $app['security.last_error']($request),
                'last_username' => $app['session']->get('_security.last_username'),
            ]);
        })->bind('login');
    
        $this->app->get('/admin/page/{id}', function ($id) use ($app) {
            return $app['twig']->render('page_view.html.twig', [
                'page' => $app['app.repos.page']->findById($id),
            ]);
        })->bind('page_by_id');
        
        $app->match('/admin/page/add', function (Request $request) use ($app) {
            $data = [
                'title' => '',
                'author' => '',
                'content' => '',
            ];
        
            /* @var Form $form */
            $form = $app['form.factory']
                ->createBuilder(FormType::class, $data)
                ->add('title')
                ->add('author')
                ->add('content', TextareaType::class)
                ->add('submit', SubmitType::class, [
                    'label' => 'Save',
                ])
                ->getForm()
            ;
            
            $form->handleRequest($request);
        
            if ($form->isValid()) {
                $data = $form->getData();
    
                /* @var Page $page */
                $page = $app['app.repos.page']
                    ->createPageFromArguments(
                        $data['title'],
                        $data['author'],
                        new \DateTime(),
                        $data['content']
                    )
                ;
                
                if (!$page) {
                    throw new \Exception('Page NOT added!');
                } else if (!$app['app.repos.page']->persist()) {
                    throw new \Exception('Page NOT persisted!');
                } else {
                    $app['session']->getFlashBag()
                        ->add('notice', 'Page saved!');
                }
            }
            
            return $app['twig']->render('page_add.html.twig', [
                'form' => $form->createView(),
            ]);
        })->bind('page_add');
        
        $app->match('/admin/page/{id}/update', function ($id, Request $request) use ($app) {
            /* @var Page $page */
            $page = $this->app['app.repos.page']->findById($id);
            
            $data = [
                'title' => $page->getTitle(),
                'slug' => $page->getSlug(),
                'author' => $page->getAuthor(),
                'content' => $page->getContent(),
            ];
            
            /* @var Form $form */
            $form = $app['form.factory']
                ->createBuilder(FormType::class, $data)
                ->add('title')
                ->add('slug', TextType::class, ['disabled' => true,])
                ->add('author')
                ->add('content', TextareaType::class)
                ->add('submit', SubmitType::class, [
                    'label' => 'Save',
                ])
                ->getForm()
            ;
            
            $form->handleRequest($request);
            
            if ($form->isValid()) {
                $data = $form->getData();
                
                if (!$app['app.repos.page']->update($id, $data)) {
                    $app['session']->getFlashBag()->add('notice',
                        'Page NOT updated!'
                    );
                } else if (!$app['app.repos.page']->persist()) {
                    $app['session']->getFlashBag()->add('notice',
                        'Page NOT persisted!'
                    );
                } else {
                    $app['session']->getFlashBag()->add('notice',
                        'Page saved!'
                    );
                    
                    $page = $app['app.repos.page']->findById($id);
                }
            }
            
            return $app['twig']->render('page_view.html.twig', [
                'page' => $page,
                'form' => $form->createView(),
            ]);
        })->bind('page_update');
    
        $this->app->delete('/admin/page/{id}/remove', function () use ($app) {
            // Must be done manually in DELETEs.
            $app['csrf.token_manager']->isTokenValid(
                new CsrfToken('token_id', 'TOKEN')
            );
            return $app->redirect('page_list');
        })->bind('page_remove');
        
        if (!$this->app['debug']) {
            $this->app->error(function (\Exception $e, Request $request, $code) {
                switch ($code) {
                    case 404:
                        $message = 'The requested page could not be found.';
                        break;
                    default:
                        $message = 'We are sorry, but something went terribly wrong.';
                }
        
                return new Response($message);
            });
        }
    }
}

namespace App\Repository;

use League\Flysystem\Filesystem;
use Silex\Application;

class PageRepository {
    const DEFAULT_LIMIT = 10;
    const PAGES_DATA_STORE = '/data/pages.json';
    
    private $app = null;
    private $filesystem = null;
    private $pages = [];
    protected $limit = null;
    
    public function __construct(
        Application $app,
        Filesystem $filesystem,
        array $pages = []
    ) {
        $this->app = $app;
        $this->filesystem = $filesystem;
        $this->createPagesFromArray($pages);
    }
    
    public function createPagesFromArray(array $pages)
    {
        for ($i = 0, $c = count($pages); $i < $c; $i++) {
            $page = new Page($pages[$i]);
            $page->setId($this->nextId());
            
            $this->add($page);
        }
    }
    
    public function createPageFromArguments(
        $title,
        $author,
        \DateTime $published_at,
        $content
    ) {
        $page_arr = [
            'title' => $title,
            'author' => $author,
            'published_at' => $published_at,
            'content' => $content,
        ];
        
        $page = new Page($page_arr);
        $page->setId($this->nextId());
        
        $this->add($page);
        
        return $page;
    }
    
    private function validate(array $page)
    {
        $have_error = [];
    
        if (!$page['title']) {
            array_push($have_error, 'title');
        }
        if (!$page['author']) {
            array_push($have_error, 'author');
        }
        if ($page['published_at']
             && (!$page['published_at'] instanceof \DateTime)
                 && !strtotime($page['published_at'])) {
            array_push($have_error, 'published_at');
        }
        if (!$page['content']) {
            array_push($have_error, 'content');
        }
        
        return !count($have_error);
    }
    
    public function hasId($id)
    {
        return !!$this->has('id', '==', $id);
    }
    
    public function hasSlug($slug)
    {
        return !!$this->has('slug', '==', $slug);
    }
    
    public function findById($id)
    {
        return $this->find('id', '==', $id, 1);
    }
    
    public function findBySlug($slug)
    {
        return $this->find('slug', '==', $slug, 1);
    }
    
    public function has($field, $operator, $value)
    {
        $value = is_string($value) ? strtoupper($value) : $value;
        
        for ($i = 0, $c = count($this->pages); $i < $c; $i++) {
            /* @var Page $page */
            $page = $this->pages[$i];
            
            if ($page->compare($field, $operator, $value)) {
                return true;
            }
        }
        
        return false;
    }
    
    public function find(
        $field,
        $operator,
        $value,
        $limit = self::DEFAULT_LIMIT
    ) {
        $limit = (int) $limit;
        $value = is_string($value) ? strtoupper($value) : $value;
        $result = [];
        
        for ($i = 0, $c = count($this->pages); $i < $c; $i++) {
            /* @var Page $page */
            $page = $this->pages[$i];
            
            if ($page->compare($field, $operator, $value)) {
                array_push($result, $page);
            }
        }
        
        if (count($result) === 1) {
            return $result[0];
        }
        
        return array_slice($result, 0, $limit);
    }
    
    public function nextId()
    {
        $max_id = array_reduce($this->pages,
            function($carry, Page $page) {
                return (int) $carry >= $page->getId()
                    ? (int) $carry
                    : $page->getId()
                ;
            }
        );
        
        $max_id++;
        
        return $max_id ?: 1;
    }
    
    public function fetch(bool $force_refresh = false)
    {
        if (!$this->pages || $force_refresh) {
            $pages = json_decode($this->filesystem->read(
                self::PAGES_DATA_STORE
            ), true);
    
            $pages = array_map(function ($page) {
                $page['published_at'] = new \DateTime(
                    $page['published_at']['date'],
                    new \DateTimeZone($page['published_at']['timezone'])
                );
        
                return $page;
            }, $pages);
    
            $this->createPagesFromArray($pages);
        }
        
        return $this->pages;
    }
    
    public function sortByDate($dir = 'ASC')
    {
        $app = $this->app;
        $sorted = $this->pages;
        
        usort($sorted, function($a, $b) use($app) {
            /* @var Page $a */ /* @var Page $b */
            if ($a->getPublishedAt() == $b->getPublishedAt()) {
                return 0;
            }
            
            return $a->getPublishedAt() > $b->getPublishedAt() ? 1 : -1;
        });
        
        if (strtoupper($dir) == 'DESC') {
            $sorted = array_reverse($sorted);
        }
        
        return $sorted;
    }
    
    public function serialize()
    {
        return array_map(function(Page $page) {
            return [
                'id' => $page->getId(),
                'title' => $page->getTitle(),
                'slug' => $page->getSlug(),
                'author' => $page->getAuthor(),
                'published_at' => $page->getPublishedAt(),
                'content' => $page->getContent(),
            ];
        }, $this->pages);
    }
    
    public function persist()
    {
        $data = $this->serialize();
        
        return $this->app['filesystem.local']->put(
            self::PAGES_DATA_STORE,
            json_encode($data)
        );
    }
    
    public function add(Page $page)
    {
        array_push($this->pages, $page);
    }
    
    public function update($id, array $page)
    {
        /* @var Page $current_page */
        $current_page = $this->app['app.repos.page']->findById($id);
        
        $page_arr = [
            'id' => $current_page->getId(),
            'title' =>
                $page['title'] != $current_page->getTitle()
                    ? $page['title']
                    : $current_page->getTitle(),
            'slug' =>
                isset($page['slug'])
                && $page['slug'] != $current_page->getSlug()
                  ? $page['slug']
                  : $current_page->getSlug(),
            'author' =>
                $page['author'] != $current_page->getAuthor()
                  ? $page['author']
                  : $current_page->getAuthor(),
            'published_at' =>
                isset($page['published_at'])
                && $page['published_at'] != $current_page->getPublishedAt()
                  ? $page['published_at']
                  : $current_page->getPublishedAt(),
            'content' =>
                $page['content'] != $current_page->getContent()
                  ? $page['content']
                  : $current_page->getContent(),
        ];
        
        if (!$this->validate($page_arr)) {
            throw new \Exception('Invalid Page information provided.');
        }
        
        $new_page = new Page($page_arr);
        
        
        if (!$this->replace($id, $new_page)) {
            throw new \Exception('Could not replace file!');
        }
        
        return $new_page;
    }
    
    private function replace($id, Page $new_page)
    {
        $pages = array_map(function(Page $page) use($id, $new_page) {
            /* @var Page $new_page */
            return $page->getId() == $id ? $new_page : $page;
        }, $this->pages);
        
        $this->pages = $pages;
        
        return true;
    }
    
    public function remove($id)
    {
        array_filter($this->pages, function ($page) use($id) {
            /* @var Page $page */
            return $page->getId() == $id ? true : false;
        });
    }
}

class Page { //implements \Serializable; { This is removed because it seems to be changing the namespace.
    private $id = null;
    private $title = '';
    private $slug = '';
    private $author = '';
    private $published_at;
    private $content = '';
    
    public function __construct(array $page)
    {
        if (!$page['published_at'] instanceof \DateTime) {
            throw new \Exception(
                'Invalid published_at date format (DateTime required).'
            );
        }
        
        $this->setId(isset($page['id']) ? $page['id'] : null);
        
        $this->title = $page['title'];
        $this->slug = isset($page['slug']) && $page['slug']
            ? $page['slug']
            : $this->sluggify($page['title'])
        ;
        $this->author = $page['author'];
        $this->published_at = $page['published_at'];
        $this->content = $page['content'];
    }
    
    public function serialize()
    {
        return json_encode([
            'id' => $this->getId(),
            'slug' => $this->getSlug(),
            'title' => $this->getTitle(),
            'author' => $this->getAuthor(),
            'published_at' => (array) $this->getPublishedAt(),
            'content' => $this->getContent(),
        ]);
    }
    
    public function unserialize(array $data)
    {
        $this->__construct(json_decode([
            'id' => $data['id'],
            'slug' => $data['slug'],
            'title' => $data['title'],
            'author' => $data['author'],
            'published_at' => new \DateTime(
                $data['published_at']['date'],
                $data['published_at']['timezone']
            ),
            'content' => $data['content'],
        ], true));
    }
    
    private function sluggify($text)
    {
        return preg_replace(
            ['/[^a-z0-9 ]/', '/[^a-z0-9]/'],
            ['', '_'],
            strtolower(trim($text))
        );
    }
    
    public function compare($field, $comparator, $value)
    {
        switch($comparator)
        {
            case '==':
                return $this->{$field} == $value;
            case '!=':
                return $this->{$field} != $value;
            case '>':
                return $this->{$field} > $value;
            case '>=':
                return $this->{$field} >= $value;
            default:
                throw new \LogicException(
                    'Operator `'.$comparator.'` invalid or unsupported '.
                    'for '.self::class.'.'
                );
        }
    }
    
    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $this->id ?: (int) $id;
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return mixed|string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    /**
     * @return mixed|string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    /**
     * @return mixed|string
     */
    public function getAuthor()
    {
        return $this->author;
    }
    
    /**
     * @return mixed
     */
    public function getPublishedAt()
    {
        return $this->published_at;
    }
    
    /**
     * @return mixed|string
     */
    public function getContent()
    {
        return $this->content;
    }
}
