serve:
	@echo "\033[32;49mServer listening on 127.0.0.1:8000\033[39m"
	@echo "Quit the server with CTRL-C."
	php -S 0.0.0.0:8080 -t web web/index_dev.php
.PHONY: serve
